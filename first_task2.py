import first_task


def main():
    print("""Select an action:
        1. Add link
        2. Show links
        3. Find link
        4. Exit""")
    while True:
        sel_action = int(input("Enter a number action: "))
        if sel_action == 1:
            while True:
                link = input("Enter a link: ")
                name = input("Enter a name link: ")
                first_task.added(name, link)
                question = str(input("Add another link? Yes or No: "))
                if question == "Yes":
                    continue
                else:
                    break
        elif sel_action == 2:
            first_task.print_dict()
        elif sel_action == 3:
            while True:
                keys = input("Enter a name link: ")
                first_task.find_link(keys)
                question_2 = str(input("Choose another link? Yes or No: "))
                if question_2 == "Yes":
                    continue
                else:
                    break
        elif sel_action == 4:
            print("You have exited the program")
            break
        else:
            print("Error")


if __name__ == "__main__":
    main()